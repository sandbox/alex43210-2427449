/**
 * @file Code button CKEditor plugin.
 */

CKEDITOR.plugins.add('codebutton',{
    requires: 'dialog',
    lang : 'en,pl',
    icons: 'code',
    onLoad : function() {
        if (CKEDITOR.config.insertpre_class) {
            CKEDITOR.addCss(
                    'pre.' + CKEDITOR.config.insertpre_class + ' {' +
                    CKEDITOR.config.insertpre_style +
                    '}'
            );
        }
    },
    init : function(editor) {
        // Allowed and required content is the same for this plugin.
        var required = CKEDITOR.config.insertpre_class ? ('pre( ' + CKEDITOR.config.insertpre_class + ' )') : 'pre';
        editor.addCommand('insertpre', new CKEDITOR.dialogCommand('insertpre', {
            allowedContent : required,
            requiredContent : required
        }));
        editor.ui.addButton && editor.ui.addButton('Code',{
            label : 'Add code',
            icon : this.path + 'icons/code.png',
            command : 'insertpre',
            toolbar: 'insert,99'
        });
        if (editor.contextMenu){
            editor.addMenuGroup('code');
            editor.addMenuItem('insertpre',{
                label : 'Edit code',
                icon : this.path + 'icons/insertpre.png',
                command : 'insertpre',
                group : 'code'
            });
            editor.contextMenu.addListener(function(element){
                if (element) {
                    element = element.getAscendant('pre', true);
                }
                if (element && !element.isReadOnly() && element.hasClass(editor.config.insertpre_class)) {
                    return { insertpre : CKEDITOR.TRISTATE_OFF };
                }
                return null;
            });
        }
        var languageItems = [];
        for(i = 0; i < drupalSettings.codebutton.languages.length; i++) {
            var language = drupalSettings.codebutton.languages[i];
            languageItems.push([language.name, language.code]);
        }
        CKEDITOR.dialog.add('insertpre', function(editor) {
            return {
                title : 'Add code',
                minWidth : 540,
                minHeight : 380,
                contents : [
                    {
                        id : 'general',
                        label : 'Code',
                        elements : [
                            {
                                type: 'select',
                                id : 'lang',
                                label: 'Language Id',
                                required : true,
                                items: languageItems,
                                setup: function(element) {
                                    this.setValue(element.getAttribute('lang'));
                                },
                                commit: function(element) {
                                    element.setAttribute('lang', this.getValue());
                                }
                            },
                            {
                                type : 'textarea',
                                id : 'contents',
                                label : 'Code',
                                cols: 140,
                                rows: 22,
                                required : true,
                                setup : function(element) {
                                    var html = element.getHtml();
                                    if (html) {
                                        var div = document.createElement('div');
                                        div.innerHTML = html;
                                        this.setValue(div.firstChild.nodeValue);
                                    }
                                },
                                commit : function(element) {
                                    element.setHtml(CKEDITOR.tools.htmlEncode(this.getValue()));
                                }
                            }
                        ]
                    }
                ],
                onShow : function() {
                    var sel = editor.getSelection();
                    var element = sel.getStartElement();
                    if (element) {
                        element = element.getAscendant('pre', true);
                    }
                    if (!element || element.getName() != 'pre' || !element.hasClass(editor.config.insertpre_class)) {
                        element = editor.document.createElement('pre');
                        this.insertMode = true;
                    }
                    else {
                        this.insertMode = false;
                    }
                    this.pre = element;
                    this.setupContent(this.pre);
                },
                onOk : function() {
                    if (editor.config.insertpre_class) {
                        this.pre.setAttribute('class', editor.config.insertpre_class);
                    }
                    if (this.insertMode) {
                        editor.insertElement(this.pre);
                    }
                    this.commitContent(this.pre);
                }
            };
        });
    }
});

if (typeof(CKEDITOR.config.insertpre_style) === 'undefined') {
  CKEDITOR.config.insertpre_style = 'background-color:#F8F8F8;border:1px solid #DDD;padding:10px;';
}
if (typeof(CKEDITOR.config.insertpre_class) === 'undefined') {
  CKEDITOR.config.insertpre_class = 'prettyprint';
}
